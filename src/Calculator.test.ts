import "jest-extended";

import {
    daysBetween,
    afterIntervalTimes,
    recurringEvent,
} from "./Calculator";

import {
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

describe("daysBetween", () => {
    test("first provided example", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const end: LocalDate = LocalDate.of(2022,1,31);
        expect(daysBetween(start, end)).toEqual(0);
    })
})

describe("daysBetween", () => {
    test("second provided example", () => {
        const start: LocalDate = LocalDate.of(2022,1,30);
        const end: LocalDate = LocalDate.of(2022,1,31);
        expect(daysBetween(start, end)).toEqual(1);
    })
})

describe("daysBetween", () => {
    test("third provided example", () => {
        const start: LocalDate = LocalDate.of(2022,1,31);
        const end: LocalDate = LocalDate.of(2022,1,30);
        expect(daysBetween(start, end)).toEqual(-1);
    })
})

describe("daysBetween", () => {
    test("fourth provided example", () => {
        const start: LocalDate = LocalDate.of(2022,1,31);
        const end: LocalDate = LocalDate.of(2022,3,31);
        expect(daysBetween(start, end)).toEqual(59);
    })
})

describe("afterIntervalTimes", () => {
    test("first provided example", () => {
        const start: LocalDate = LocalDate.of(2022,1,31);
        const interval: Period = Period.of(0,0,1);
        const multiplier: number = 3;
        const result: LocalDate = LocalDate.of(2022,2,3);
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(result);
    })
});

describe("afterIntervalTimes", () => {
    test("second provided example", () => {
        const start: LocalDate = LocalDate.of(2022,1,31);
        const interval: Period = Period.of(0,0,1);
        const multiplier: number = 0;
        const result: LocalDate = LocalDate.of(2022,1,31);
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(result);
    })
});

describe("afterIntervalTimes", () => {
    test("third provided example", () => {
        const start: LocalDate = LocalDate.of(2022,1,31);
        const interval: Period = Period.of(0,1,0);
        const multiplier: number = 1;
        const result: LocalDate = LocalDate.of(2022,2,28);
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(result);
    })
});

describe("afterIntervalTimes", () => {
    test("fourth provided example", () => {
        const start: LocalDate = LocalDate.of(2019,1,31);
        const interval: Period = Period.of(1,1,0);
        const multiplier: number = 1;
        const result: LocalDate = LocalDate.of(2020,2,29);
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(result);
    })
});

describe("recurringEvent", () => {
    test("first provided example", () => {
        const start: LocalDateTime = LocalDateTime.of(2022,1,1,0,0,0,0);
        const end: LocalDateTime = LocalDateTime.of(2022,1,4,23,59,0,0);
        const interval: Period = Period.of(0,0,1);
        const timeOfDay: LocalTime = LocalTime.of(1,0,0,0);
        const result: LocalDateTime[] = [
            LocalDateTime.of(2022,1,1,1,0,0,0),
            LocalDateTime.of(2022,1,2,1,0,0,0),
            LocalDateTime.of(2022,1,3,1,0,0,0),
            LocalDateTime.of(2022,1,4,1,0,0,0),
        ];
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual(result);
    })
});

describe("recurringEvent", () => {
    test("second provided example", () => {
        const start: LocalDateTime = LocalDateTime.of(2022,1,2,0,0,0,0);
        const end: LocalDateTime = LocalDateTime.of(2022,1,4,23,59,0,0);
        const interval: Period = Period.of(0,0,1);
        const timeOfDay: LocalTime = LocalTime.of(1,0,0,0);
        const result: LocalDateTime[] = [
            LocalDateTime.of(2022,1,2,1,0,0,0),
            LocalDateTime.of(2022,1,3,1,0,0,0),
            LocalDateTime.of(2022,1,4,1,0,0,0),
        ];
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual(result);
    })
});

describe("recurringEvent", () => {
    test("third provided example", () => {
        const start: LocalDateTime = LocalDateTime.of(2022,1,1,0,0,0,0);
        const end: LocalDateTime = LocalDateTime.of(2022,1,4,0,0,0,0);
        const interval: Period = Period.of(0,0,1);
        const timeOfDay: LocalTime = LocalTime.of(1,0,0,0);
        const result: LocalDateTime[] = [
            LocalDateTime.of(2022,1,1,1,0,0,0),
            LocalDateTime.of(2022,1,2,1,0,0,0),
            LocalDateTime.of(2022,1,3,1,0,0,0),
        ];
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual(result);
    })
});

describe("recurringEvent", () => {
    test("fourth provided example", () => {
        const start: LocalDateTime = LocalDateTime.of(2022,1,31,0,0,0,0);
        const end: LocalDateTime = LocalDateTime.of(2022,5,15,0,0,0,0);
        const interval: Period = Period.of(0,1,0);
        const timeOfDay: LocalTime = LocalTime.of(1,0,0,0);
        const result: LocalDateTime[] = [
            LocalDateTime.of(2022,1,31,1,0,0,0),
            LocalDateTime.of(2022,2,28,1,0,0,0),
            LocalDateTime.of(2022,3,28,1,0,0,0),
            LocalDateTime.of(2022,4,28,1,0,0,0),
        ];
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual(result);
    })
});