import {
    ChronoUnit,
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

/**
 * @param {!LocalDate} start the base LocalDate object.
 * @param {!LocalDate} end the other LocalDate object.
 * @return {number} the period between start and end in terms of days;
 *  positive if start is later than end, negative if earlier.
 *  start date is include, end date is exclusive.
 *  Utilizes the ChronoUnit class, found from ChronoUnit
 */
export function daysBetween(
    start: LocalDate,
    end: LocalDate,
): number {
    return ChronoUnit.DAYS.between(start, end);
}

/**
 * @param {!LocalDate} start the base LocalDate object.
 * @param {!Period} end the Period object of time.
 * @param {!number} multiplier the number of Periods to multiply by.
 * @return {LocalDate} Returns a LocalDate object obtained by starting
 *  at start date and adding the interval period multiplier times.
 *  Utilizes the Period class' multipliedBy method, found from:
 *  Period class and instance-method-multipliedBy
 */
export function afterIntervalTimes(
    start: LocalDate,
    interval: Period,
    multiplier: number,
): LocalDate {
    return start.plus(interval.multipliedBy(multiplier));
}

/**
* @param {!LocalDateTime} start - The base LocalDateTime object.
* @param {!LocalDateTime} end - The other LocalDateTime object.
* @param {!Period} interval - The interval between recurring events.
* @param {!LocalDate} timeOfDay - The time of day for the recurring events.
* @returns {LocalDateTime[]} - Array of dateTimes that recurring event happens.
*
* Calculates the recurring events between start and end LocalDateTimes.
* The event will take place once per interval at the specified timeOfDay.
* If the start time is earlier in the day than timeOfDay, the first event will
* take place on the start date. Otherwise, the first event will take place
* exactly one interval after the start date.
* Similarly, if the end time is later in the day than timeOfDay and the last
* interval ends exactly on the end date,
* the last event should take place on the end date.
* Otherwise, the last event should take place at most one interval before
* the end date. In all cases, the time component of every element in the
* output array will be equal to timeOfDay.
* Utilizes the following methods:
* LocalDateTime.html#instance-method-toLocalTime
* LocalDateTime.html#instance-method-isBefore
* LocalDateTime.html#instance-method-isEqual
* "with" is inherited by the Temporal class:
* Temporal.html#instance-method-with
* Temporal.html#instance-method-plus
* From the arrays class:
* Global_Objects/Array/push
*/
export function recurringEvent(
    start: LocalDateTime,
    end: LocalDateTime,
    interval: Period,
    timeOfDay: LocalTime,
): LocalDateTime[] {
    const eventTimes: LocalDateTime[] = [];

    // Initializes the currentDateTime to the start time
    let currentDateTime = start;

    // Checks and adds start if its time is earlier in the day than timeOfDay
    if (currentDateTime.toLocalTime().isBefore(timeOfDay)) {
        // Adds the currentDateTime to the array
        const eventDateTime: LocalDateTime = currentDateTime.with(timeOfDay);
        eventTimes.push(eventDateTime);
    }

    // Advances the currentDateTime to the next interval
    currentDateTime = currentDateTime.plus(interval);

    // Iterates over intervals and addsdateTimes to eventTimes array
    while (currentDateTime.isBefore(end)) {
        // Adds the currentDateTime to the array
        const eventDateTime: LocalDateTime = currentDateTime.with(timeOfDay);
        eventTimes.push(eventDateTime);

        // Advances the currentDateTime to the next interval
        currentDateTime = currentDateTime.plus(interval);
    }

    // Returns the array of LocalTimes
    return eventTimes;
}